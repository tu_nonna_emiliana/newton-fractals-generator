// read Reverse Polish Notation
#include "vector"
#include "stack"

// eval
#include "iostream"
#include "map"
#include "string"
#include "complex"
#include "functional"

#define complex complex<double>
#define FunctionMap map<string, function<complex(complex)>>
#define OperatorMap map<string, function<complex(complex, complex)>>  

using namespace std;
namespace local {
	// functions
	complex cos (const complex & z) { return std::cos(z); }
	complex cosh (const complex & z) { return std::cosh(z); }
	complex exp (const complex & z) { return std::exp(z); }
	complex log (const complex & z) { return std::log(z); }
	complex log10 (const complex & z) { return std::log10(z); }
	complex sin (const complex & z) { return std::sin(z); }
	complex sinh (const complex & z) { return std::sinh(z); }
	complex sqrt (const complex & z) { return std::sqrt(z); }
	complex tan (const complex & z) { return std::tan(z); }
	complex tanh (const complex & z) { return std::tanh(z); }
	complex acos (const complex & z) { return std::acos(z); }
	complex acosh (const complex & z) { return std::acosh(z); }
	complex asin (const complex & z) { return std::asin(z); }
	complex asinh (const complex & z) { return std::asinh(z); }
	complex atan (const complex & z) { return std::atan(z); }
	complex atanh (const complex & z) { return std::atanh(z); }
	
	//operators
	complex plus (const complex & z0, const complex & z1) { return z0 + z1; }
	complex minus (const complex & z0, const complex & z1) { return z0 - z1; }
	complex multiplies (const complex & z0, const complex & z1) { return z0 * z1; }
	complex divides (const complex & z0, const complex & z1) { return z0 / z1; }
	// complex modulus (complex z0, complex z1) { return z0 % z1; } // it doesn't exist
	complex pow (const complex & z0, const complex & z1) { return std::pow(z0,z1); }
}

void defineFunctions (FunctionMap & functions);
void defineOperators (OperatorMap & operators);

vector <string> tokenize (string & expr, FunctionMap & functions, OperatorMap & operators);
complex eval (vector <string> & tokens, FunctionMap & functions, OperatorMap & operators);
complex parseComplex(string & token);

int main() {
	stack <int> s;

	FunctionMap functions;
	OperatorMap operators;
	
	defineFunctions(functions);
	defineOperators(operators);

	// reverse polish notation
	string expr = "";
	// get line
	while(true) {
		char c;
		cin.get(c);
		if (c == '\n') break;
		expr+=c;
	}
	vector <string> tokens = tokenize(expr, functions, operators);
	
	// for (auto it : tokens) cout << it << endl;
	// eval reverse polish notation
	
	cout << eval (tokens, functions, operators) << endl;
	
	return 0;
}

void defineFunctions (FunctionMap & functions) {
	functions["cos"] = local::cos;
	functions["cosh"] = local::cosh;
	functions["exp"] = local::exp;
	functions["ln"] = local::log;
	functions["log"] = local::log;
	functions["log10"] = local::log10;
	functions["sin"] = local::sin;
	functions["sinh"] = local::sinh;
	functions["sqrt"] = local::sqrt;
	functions["tan"] = local::tan;
	functions["tanh"] = local::tanh;
	functions["acos"] = local::acos;
	functions["acosh"] = local::acosh;
	functions["asin"] = local::asin;
	functions["asinh"] = local::asinh;
	functions["atan"] = local::atan;
	functions["atanh"] = local::atanh;
}

void defineOperators (OperatorMap & operators) {
	operators["+"] = local::plus;
	operators["-"] = local::minus;
	operators["*"] = local::multiplies;
	operators["/"] = local::divides; 
	// operators["%"] = local::modulus; // it doesn't exist, lol
	operators["^"] = local::pow; 
	// operators["pow"] = local::pow;
}

vector <string> tokenize (string & expr, FunctionMap & functions, OperatorMap & operators) {
	int len = expr.size();
	vector <string> tokens;
	for (int i=0; i<len; ++i) {
		// skip spaces
		if (expr[i] == ' ') continue;
		// read numbers
		else if (expr[i] == '(') {
			int end = -i;
			for (int j=i; j!=len; ++j) if (expr[j] == ')') {end = j; break;}
			if (end == -i) exit(EXIT_FAILURE);
			tokens.push_back(expr.substr(i, end-i+1));
			i = end;
		}
		// read operators
		else if (operators.find(expr.substr(i,1)) != operators.end()) {
			tokens.push_back(expr.substr(i,1));
		}
		// read functions
		else {
			string func = "";
			int j=i;
			for(j=i; j!=len; ++j) {
				if (expr[j] == ' ') break;
				else func+=expr[j];
			}
			i=j;
			if (functions.find(func) != functions.end()) tokens.push_back(func);
			// else exit(EXIT_FAILURE);
		}
	}
	return tokens;
}

complex eval(vector<string> & tokens,FunctionMap & functions,OperatorMap & operators){
	int len = tokens.size();
	stack <complex> nums;
	for (int i=0; i<len; ++i) {
		// check function
		if (functions.find(tokens[i]) != functions.end()) {
			complex a = nums.top();
			nums.pop();
			nums.push(functions[tokens[i]](a));
			// cout << "vive e lotta assieme a noi in function " << endl;
		}
		// check operator
		else if (operators.find(tokens[i]) != operators.end()) {
			complex a = nums.top();
			nums.pop();
			complex b = nums.top();
			nums.pop();
			complex n = operators[tokens[i]](b,a);
			// cout << n << endl;
			nums.push(n);
			// cout << "vive e lotta assieme a noi in operator " << endl;
		}
		// check number
		else {
			complex c = parseComplex(tokens[i]);
			nums.push(c);
			// cout << "vive e lotta assieme a noi in number " << endl;
		}
	}
	return nums.top();
}

complex parseComplex (string & token) {
	if (token[0] != '(' || token[token.size()-1] != ')') exit(EXIT_FAILURE);
	int commaIndex = 0;
	bool sign[] = {false, false};
	if (token[1]=='-' || token[1]=='+') sign[0] = true;
	for (int i=1; i!=token.size()-1; ++i) {
		if (token[i]==',') {
			commaIndex = i;
			if(token[commaIndex]=='-' || token[commaIndex]=='+') 
				sign[1] = true;
		}
	}
	double a = atof(token.substr(1,commaIndex-1).c_str());
	double b = atof(token.substr(commaIndex+1, token.size()-2-commaIndex).c_str());
	complex c (a,b);
	return c;
}
