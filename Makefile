# Usage:
# make        # compile all binary
# make clean  # remove ALL binaries and objects

.PHONY = clean

CXX = g++                        # compiler to use

LINKERFLAG = -lm

SRCS := $(wildcard *.cpp)
BINS := $(SRCS:%.o=%)

BINNAME := NewtonOnTheRocks

%: ${BINS}
	@echo "Checking.."
	${CXX} ${LINKERFLAG} $^ -o ${BINNAME}

%.o: ${SRCS}
	@echo "Creating object.."
	${CXX} -c $^

clean:
	@echo "Cleaning up..."
	rm -rvf *.o $.{BINNAME}
